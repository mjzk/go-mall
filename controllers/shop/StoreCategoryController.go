package shop

import (
	"encoding/json"
	"github.com/beego/beego/v2/core/logs"
	"yixiang.co/yshop/controllers"
	"yixiang.co/yshop/models"
	"yixiang.co/yshop/models/vo"
)

// 商品分类api
type StoreCategoryController struct {
	controllers.BaseController
}

func (c *StoreCategoryController) URLMapping() {
	c.Mapping("Post", c.Post)
	c.Mapping("GetAll", c.GetAll)
	c.Mapping("Put", c.Put)
	c.Mapping("Delete", c.Delete)
}

// @Title 商品分类列表
// @Description 商品分类列表
// @Success 200 {object} controllers.Result
// @router / [get]
func (c *StoreCategoryController) GetAll() {
	name := c.GetString("name")
	enabled, _ := c.GetInt8("enabled", -1)
	list := models.GetAllCates(name, enabled)
	c.Ok(vo.ResultList{Content: list, TotalElements: 0})
}

// @Title 添加商品分类
// @Description 添加商品分类
// @Success 200 {object} controllers.Result
// @router / [post]
func (c *StoreCategoryController) Post() {
	var model models.YshopStoreCategory
	json.Unmarshal(c.Ctx.Input.RequestBody, &model)
	c.Valid(&model)
	_, e := models.AddCate(&model)
	if e != nil {
		c.Fail(e.Error(), 5002)
	}
	c.Ok("操作成功")
}

// @Title 修改商品分类
// @Description 修改商品分类
// @Success 200 {object} controllers.Result
// @router / [put]
func (c *StoreCategoryController) Put() {
	var model models.YshopStoreCategory
	json.Unmarshal(c.Ctx.Input.RequestBody, &model)
	c.Valid(&model)
	e := models.UpdateByCate(&model)
	if e != nil {
		c.Fail(e.Error(), 5004)
	}
	c.Ok("操作成功")
}

// @Title 删除商品分类
// @Description 删除商品分类
// @Success 200 {object} controllers.Result
// @router / [delete]
func (c *StoreCategoryController) Delete() {
	var ids []int64
	json.Unmarshal(c.Ctx.Input.RequestBody, &ids)
	logs.Info(ids)
	e := models.DelByCate(ids)
	if e != nil {
		c.Fail(e.Error(), 5005)
	}
	c.Ok("操作成功")
}
