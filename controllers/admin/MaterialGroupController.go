package admin

import (
	"encoding/json"
	"yixiang.co/yshop/controllers"
	"yixiang.co/yshop/models"
)

// 素材分组api
type MaterialGroupController struct {
	controllers.BaseController
}

func (c *MaterialGroupController) URLMapping() {
	c.Mapping("Post", c.Post)
	c.Mapping("GetAll", c.GetAll)
	c.Mapping("Put", c.Put)
	c.Mapping("Delete", c.Delete)
}

// @Title 素材分组列表
// @Description 素材分组列表
// @Success 200 {object} controllers.Result
// @router / [get]
func (c *MaterialGroupController) GetAll() {
	list := models.GetAllGroup("")
	c.Ok(list)
}

// @Title素材分组添加
// @Description素材分组添加
// @Success 200 {object} controllers.Result
// @router / [post]
func (c *MaterialGroupController) Post() {
	var model models.SysMaterialGroup
	json.Unmarshal(c.Ctx.Input.RequestBody, &model)
	c.Valid(&model)
	_, e := models.AddMaterialGroup(&model)
	if e != nil {
		c.Fail(e.Error(), 5002)
	}
	c.Ok("操作成功")
}

// @Title 素材分组修改
// @Description 素材分组修改
// @Success 200 {object} controllers.Result
// @router / [put]
func (c *MaterialGroupController) Put() {
	var model models.SysMaterialGroup
	json.Unmarshal(c.Ctx.Input.RequestBody, &model)
	c.Valid(&model)
	e := models.UpdateByMaterialGroup(&model)
	if e != nil {
		c.Fail(e.Error(), 5004)
	}
	c.Ok("操作成功")
}

// @Title 素材分组删除
// @Description 素材分组删除
// @Success 200 {object} controllers.Result
// @router /:id [delete]
func (c *MaterialGroupController) Delete() {
	var ids []int64
	id, _ := c.GetInt64(":id", 0)
	ids = append(ids, id)
	e := models.DelByMaterialGroup(ids)
	if e != nil {
		c.Fail(e.Error(), 5005)
	}
	c.Ok("操作成功")
}
