module yixiang.co/yshop

go 1.15

require github.com/beego/beego/v2 v2.0.1

require (
	github.com/beego/bee/v2 v2.0.2 // indirect
	github.com/casbin/beego-orm-adapter/v3 v3.0.0
	github.com/casbin/casbin/v2 v2.38.0
	github.com/dgrijalva/jwt-go v3.2.0+incompatible
	github.com/go-sql-driver/mysql v1.5.0
	github.com/gomodule/redigo v2.0.0+incompatible
	github.com/jinzhu/copier v0.3.2
	github.com/mojocn/base64Captcha v1.3.1
	github.com/robfig/cron/v3 v3.0.1
	github.com/segmentio/ksuid v1.0.4
	github.com/smartystreets/goconvey v1.6.4
	golang.org/x/crypto v0.0.0-20200622213623-75b288015ac9
	golang.org/x/image v0.0.0-20201208152932-35266b937fa6 // indirect
)
