package models

import (
	"github.com/beego/beego/v2/client/orm"
	"github.com/segmentio/ksuid"
	"sort"
	"strconv"
	"strings"
	"yixiang.co/yshop/common/untils"
	"yixiang.co/yshop/models/dto"
)

type YshopStoreProductAttrValue struct {
	Id           int64   `json:"id"`
	ProductId    int64   `json:"productId" valid:"Required;"`
	Sku          string  `json:"sku" valid:"Required;"`
	Stock        int     `json:"stock" valid:"Required;"`
	Sales        int     `json:"sales"`
	Price        float64 `json:"price"`
	Image        string  `json:"image"`
	Unique       string  `json:"unique"`
	Cost         float64 `json:"cost"`
	BarCode      string  `json:"barCode"`
	OtPrice      float64 `json:"otPrice"`
	Weight       float64 `json:"weight"`
	Volume       float64 `json:"volume"`
	Brokerage    float64 `json:"brokerage"`
	BrokerageTwo float64 `json:"brokerage"`
	PinkPrice    float64 `json:"pinkPrice"`
	PinkStock    int     `json:"pinkStock"`
	SeckillPrice float64 `json:"seckillPrice"`
	SeckillStock int     `json:"seckillStock"`
	Integral     int     `json:"integral"`
}

func init() {
	orm.RegisterModel(new(YshopStoreProductAttrValue))
}

func SelectAllProductAttrValues( productId int64) (data []*YshopStoreProductAttrValue){
	o := orm.NewOrm()
	o.QueryTable(new(YshopStoreProductAttrValue)).Filter("product_id", productId).All(data)

	return data
}

func GetAttrValueByProductIdAndSku(productId int64, sku string) *YshopStoreProductAttrValue {
	o := orm.NewOrm()
	var attrValue YshopStoreProductAttrValue
	o.QueryTable(new(YshopStoreProductAttrValue)).Filter("product_id", productId).Filter("sku", sku).One(&attrValue)

	return &attrValue
}

func AddProductttrValue(attrs []dto.ProductFormat, productId int64) (err error) {
	o := orm.NewOrm()
	var valueGroup []YshopStoreProductAttrValue
	for _, val := range attrs {
		stringList := untils.GetValues(val.Detail)
		sort.Strings(stringList)
		str := strings.Join(stringList, ",")
		price, _ := strconv.ParseFloat(val.Price, 64)
		cost, _ := strconv.ParseFloat(val.Cost, 64)
		otPrice, _ := strconv.ParseFloat(val.OtPrice, 64)
		weight, _ := strconv.ParseFloat(val.Weight, 64)
		volume, _ := strconv.ParseFloat(val.Volume, 64)
		brokerage, _ := strconv.ParseFloat(val.Brokerage, 64)
		brokerageTwo, _ := strconv.ParseFloat(val.BrokerageTwo, 64)
		stock, _ := strconv.Atoi(val.Stock)
		uuid := ksuid.New()
		var storeProductAttrValue = YshopStoreProductAttrValue{
			ProductId:    productId,
			Sku:          str,
			Price:        price,
			Cost:         cost,
			OtPrice:      otPrice,
			Unique:       uuid.String(),
			Image:        val.Pic,
			BarCode:      "",
			Weight:       weight,
			Volume:       volume,
			Brokerage:    brokerage,
			BrokerageTwo: brokerageTwo,
			Stock:        stock,
			PinkStock:    0,
			PinkPrice:    0,
			SeckillStock: 0,
			SeckillPrice: 0,
		}
		valueGroup = append(valueGroup, storeProductAttrValue)
	}

	_, err = o.InsertMulti(100, valueGroup)

	return err
}

func DelByProductttrValue(productId int64) (err error) {
	o := orm.NewOrm()
	qs := o.QueryTable(new(YshopStoreProductAttrValue))
	_, err = qs.Filter("product_id", productId).Delete()
	return err
}
