package models

import (
	"github.com/beego/beego/v2/client/orm"
	"github.com/beego/beego/v2/core/logs"
	"strconv"
	"yixiang.co/yshop/common/untils"
	"yixiang.co/yshop/models/dto"
)

type SysMaterial struct {
	Id       int64  `json:"id"`
	Name     string `json:"name" valid:"Required;"`
	Type     string `json:"type"`
	Url      string `json:"url"`
	GroupId  int64  `json:"groupId"`
	CreateId int64  `json:"create_id"`
	BaseModel
}

func init() {
	orm.RegisterModel(new(SysMaterial))
}

func GetAllMaterial(base dto.BasePage, query ...interface{}) (int, []SysMaterial) {
	var (
		tableName = "sys_material"
		data      []SysMaterial
		condition = ""
	)
	if base.Blurry != "" {
		condition = " and name= '" + base.Blurry + "'"
	}
	if len(query) > 0 {
		groupId := query[0].(int64)
		if groupId > 0 {
			condition += " and group_id=" + strconv.FormatInt(groupId, 10)
		}
	}
	total, _, rs := GetPagesInfo(tableName, base.Page, base.Size, condition)
	rs.QueryRows(&data)

	return total, data
}

func AddMaterial(m *SysMaterial) (id int64, err error) {
	o := orm.NewOrm()
	id, err = o.Insert(m)
	return
}

func UpdateByMaterial(m *SysMaterial) (err error) {
	o := orm.NewOrm()
	_, err = o.Update(m)
	return
}

func DelByMaterial(ids []int64) (err error) {
	str := untils.ReturnQ(len(ids))
	logs.Info(str)
	o := orm.NewOrm()
	_, err = o.Raw("UPDATE sys_material SET is_del = ? WHERE id in("+str+")", 1, ids).Exec()
	return
}
