package models

import (
	"github.com/beego/beego/v2/client/orm"
	"github.com/beego/beego/v2/core/logs"
	"yixiang.co/yshop/common/untils"
)

type SysMaterialGroup struct {
	Id       int64  `json:"id"`
	Name     string `json:"name" valid:"Required;"`
	CreateId int64  `json:"create_id"`
	BaseModel
}

func init() {
	orm.RegisterModel(new(SysMaterialGroup))
}

func GetAllGroup(name string) []SysMaterialGroup {
	var data []SysMaterialGroup
	o := orm.NewOrm()
	qs := o.QueryTable("sys_material_group").Filter("is_del", 0)
	if name != "" {
		qs = qs.Filter("name", name)
	}

	qs.All(&data)
	return data
}

func AddMaterialGroup(m *SysMaterialGroup) (id int64, err error) {
	o := orm.NewOrm()
	id, err = o.Insert(m)
	return
}

func UpdateByMaterialGroup(m *SysMaterialGroup) (err error) {
	o := orm.NewOrm()
	_, err = o.Update(m)
	return
}

func DelByMaterialGroup(ids []int64) (err error) {
	str := untils.ReturnQ(len(ids))
	logs.Info(str)
	o := orm.NewOrm()
	_, err = o.Raw("UPDATE sys_material_group SET is_del = ? WHERE id in("+str+")", 1, ids).Exec()
	return
}
