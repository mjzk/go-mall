package models

import (
	"github.com/beego/beego/v2/client/orm"
	"strings"
	"yixiang.co/yshop/models/dto"
)

type YshopStoreProductAttr struct {
	Id         int64  `json:"id"`
	ProductId  int64  `json:"productId" valid:"Required;"`
	AttrName   string `json:"attrName" valid:"Required;"`
	AttrValues string `json:"attrValues" valid:"Required;"`
}

func init() {
	orm.RegisterModel(new(YshopStoreProductAttr))
}

func AddProductAttr(items []dto.FormatDetail, productId int64) (err error) {
	o := orm.NewOrm()
	var attrGroup []YshopStoreProductAttr
	for _, val := range items {
		detailStr := strings.Join(val.Detail, ",")
		var storeProductAttr = YshopStoreProductAttr{
			ProductId:  productId,
			AttrName:   val.Value,
			AttrValues: detailStr,
		}
		attrGroup = append(attrGroup, storeProductAttr)
	}

	_, err = o.InsertMulti(100, attrGroup)

	return err
}

func DelByProductttr(productId int64) (err error) {
	o := orm.NewOrm()
	qs := o.QueryTable(new(YshopStoreProductAttr))
	_, err = qs.Filter("product_id", productId).Delete()
	return err
}
